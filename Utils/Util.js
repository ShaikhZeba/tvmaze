
import React from "react";
import Snackbar from "react-native-snackbar";

export let showSnackBar = (text, type) => {
  Snackbar.show({
    text: text,
    backgroundColor: type === "success" ? "#00A444" : "#A3423F",
    duration: Snackbar.LENGTH_INDEFINITE,
    action: {
      text: "OK",
      textColor: "#fff"
    }
  });
};
