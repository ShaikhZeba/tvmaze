import moment from 'moment';
export function GetBaseURL() {
  return 'http://api.tvmaze.com/';
}
export const URLs = {
  SHOW_LIST: {
    URL: `schedule?<<countryName>>date=${moment().format('YYYY-MM-DD')}`,
  },
};
