//Common API calling function
import {GetBaseURL} from '@networking/Urls'; //Base Url of API
import Axios from 'axios';

let axiosInstance;
const connectionTimeout = 60000; // Request will be cancelled after 60 second

async function getAxiosInstance() { // Creating axios instance with some default configuration
  axiosInstance = Axios.create({ // We can read JWT token and pass it in header as a default setting
    baseURL: GetBaseURL(),
    timeout: connectionTimeout,
  });

  return axiosInstance;
}

export const apiCall = async (request, payload) => {
  let {URL} = request; //URL page from the Screen(Page)
  const axiosInstance = await getAxiosInstance(); //get the axios instance 
  try {
    const response = await axiosInstance.get(URL, payload);// API calling with get request

    if (response.status === 200) {

      if (response.data) {
        return {               // Returning data from API
          apiSuccess: true,
          data: response.data,
        };
      } else {
        return {apiSuccess: false, data: response.data}; //Returning failed response 
      }
    } else {
      return {apiSuccess: false, data: response}; //Returning failed response 
    }
  } catch (error) {
console.log("error?.response",error?.response.data);
    if (error.code == 'ECONNABORTED') {
      return {
        data: {message: 'Something when wrong. Try again later.'}, 
        apiSuccess: false,
      };
    } else {
      return {
        data: error?.response?.data || error, //Returning failed response 
        apiSuccess: false,
      };
    }
  }
};
