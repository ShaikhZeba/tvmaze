import Navigator from '@navigation/Router'; //
import rootReducer from '@redux/RootReducer';
import React, {useEffect} from 'react';
import {StatusBar, View} from 'react-native';
import {Provider} from 'react-redux';
import {applyMiddleware, createStore} from 'redux';
import thunk from 'redux-thunk';
function App() {
  const store = createStore(rootReducer, applyMiddleware(thunk));
  return (
    <>
      <StatusBar
        translucent={false}
        backgroundColor="#7E354D"
        barStyle="light-content"
      />
      <Provider store={store}>
        <Navigator />
      </Provider>
    </>
  );
}
export default App;
