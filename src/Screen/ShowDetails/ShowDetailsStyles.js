//Styles Sheet of Show details Page
import {StyleSheet} from 'react-native';
const styles = StyleSheet.create({
  mainContainer: {flex: 1, backgroundColor: '#fff'},
  flex1: {flex: 1},
  headerContainer: {
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerText: {
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#7D0552',
  },
  imageContainer: {flex: 0.3},
  imagView: {
    width: '100%',
    height: 300,
    borderWidth: 1,
  },
  emptyImage: {justifyContent: 'center', alignItems: 'center'},
  textContainer: {padding: 10},
  textStyles: {
    fontSize: 20,
    fontFamily: 'Montserrat',
    color: '#7D0552',
    fontWeight: 'bold',
  },
  smallTextStyles: {
    fontSize: 14,
    fontFamily: 'Montserrat',
    color: '#7D0552',
  },
  ratContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 10,
    alignItems: 'center',
  },
  ratView: {
    backgroundColor: '#7D0552',
    padding: 8,
    borderRadius: 10,
  },
  rateText: {flexDirection: 'row', alignItems: 'center'},
  btnView: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-around',
    paddingVertical: 20,
  },
  btnColor: {
    width: '90%',
    borderWidth: 0.5,
    borderRadius: 10,
    padding: 10,
    margin: 10,
    borderColor: '#7D0552',
  },
});

export default styles;
