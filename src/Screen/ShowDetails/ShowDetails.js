import React, {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  Button,
  ScrollView,
} from 'react-native';
import Left from '@icons/Back.svg';
import LocalStyles from '@showDetails/ShowDetailsStyles';
import TvIcon from '@icons/tv.svg';
import StartIcon from '@icons/fillStarIcon.svg';
import moment from 'moment';

export default function ShowDetails(props) {
  const [is_Loading, setIs_Loading] = useState(true);
  const [localData, setLocalData] = useState(props?.route?.params.item); //Param Data from Show list page

  const getSummary = (text) => {
    // Replace HTML Tags
    const regex = /(<([^>]+)>)/gi;
    const result = text.replace(regex, '');
    return result;
  };
  const {
    mainContainer,
    flex1,
    headerContainer,
    textStyles,
    headerText,
    smallTextStyles,
    imageContainer,
    imagView,
    emptyImage,
    textContainer,
    ratContainer,
    ratView,
    rateText,
    btnView,
    btnColor,
  } = LocalStyles;

  return (
    <ScrollView style={mainContainer}>
      <View style={flex1}>
        <View style={headerContainer}>
          <TouchableOpacity
            activeOpacity={0.8}
            style={{paddingHorizontal: 5}}
            onPress={() => props?.navigation.goBack()}>
            <Left width={30} height={30} />
          </TouchableOpacity>
          <Text style={headerText}>{localData.show.name}</Text>
        </View>
        <View style={imageContainer}>
          {localData?.show?.image?.medium ? (
            <Image
              source={{
                uri: localData?.show?.image?.medium,
              }}
              style={imagView}
              resizeMode={'stretch'}
              onLoadStart={() => setIs_Loading(true)}
              onLoadEnd={() => setIs_Loading(false)}
            />
          ) : (
            <View style={emptyImage}>
              <TvIcon width={130} height={130} />
            </View>
          )}
        </View>
        <View style={textContainer}>
          <Text style={textStyles}>{localData?.show?.name}</Text>
        </View>
        <View style={textContainer}>
          <Text style={smallTextStyles}>
            {localData?.show?.type} {' | '} {localData?.show?.language}
            {' | '}
            {localData?.show?.genres
              ? localData?.show?.genres.toString()
              : null}
            {' | '} {moment(localData?.show?.premiered).format('YY-DD')}
          </Text>
        </View>
        <View style={ratContainer}>
          {localData?.show?.schedule?.days && (
            <Text style={smallTextStyles}>
              {localData?.show?.schedule?.days?.length > 1
                ? 'Days | '
                : 'Day | '}
              {localData?.show?.schedule?.days?.toString()}
            </Text>
          )}

          {localData.show?.rating?.average && (
            <View style={ratView}>
              <View style={rateText}>
                <StartIcon />
                <Text
                  style={{
                    ...smallTextStyles,
                    color: '#fff',
                    marginHorizontal: 5,
                  }}>
                  {localData?.show?.rating?.average}
                </Text>
              </View>
            </View>
          )}
        </View>
        <View style={textContainer}>
          <Text
            style={{
              ...smallTextStyles,
              textAlign: 'justify',
              marginHorizontal: 5,
            }}>
            {getSummary(localData?.show?.summary)}
          </Text>
        </View>
      </View>
      {/* // demo Button  */}
      <View style={btnView}>
        <TouchableOpacity activeOpacity={0.8} style={btnColor}>
          <Button title={'Watch Now'} color="#7D0552" />
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
}
