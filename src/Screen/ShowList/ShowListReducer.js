import {
  SHOW_LIST_LOADING, //Set loader on page (UI)
  SHOW_LIST_SUCCESS, //Indicate API executed successfully
  SHOW_LIST_FAILURE, //Indicate API ecection failed
} from '@redux/Types'; // Type of Action

const initialState = {
  showListError: null, //Error message from API
  showListLoading: true, //Set loader on page (UI)
  showListData: [], // Data from API
};

const ShowListReducer = (state = initialState, action) => {
  switch (action.type) {
    case SHOW_LIST_LOADING:
      return {
        ...state,
        showListLoading: true,
      };

    case SHOW_LIST_FAILURE:
      return {
        ...state,
        showListLoading: false,
        showListError: action.payload.data.message,
      };

    case SHOW_LIST_SUCCESS:
      return {
        ...state,
        showListError: null,
        showListLoading: false,
        showListData: action.payload?.data,
      };

    default:
      return state;
  }
};

export default ShowListReducer;
