//Styles Sheet of Show List Page

import {StyleSheet} from 'react-native';
const styles = StyleSheet.create({
  card: {
    borderRadius: 15,
    backgroundColor: '#810541',
    elevation: 10,
    margin: 10,
  },
  textStyles: {
    fontSize: 18,
    fontFamily: 'Montserrat',
    color: '#fff',
    fontWeight: 'bold',
    marginHorizontal: 5,
  },
  containerCenter: {flex: 1, justifyContent: 'center', alignItems: 'center'},
  noDataImg: {maxHeight: 200, maxWidth: 200},
  noDataText: {
    borderWidth: 1,
    borderRadius: 10,
    padding: 10,
    borderColor: '#7D0552',
  },
  mainFlex: {flex: 1, backgroundColor: '#fff'},
  headerContainer: {
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  titleText: {
    textAlign: 'left',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#7D0552',
  },
  paddingText: {padding: 10},
  listContainer: {paddingBottom: 20},
  listImg: {
    width: '100%',
    height: 120,
    borderWidth: 1,
  },
  noDataImag: {justifyContent: 'center', alignItems: 'center'},
  textContainer: {
    marginVertical: 5,
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  backView: {
    alignItems: 'flex-end',
    backgroundColor: '#fff',
    padding: 10,
    borderRadius: 10,
    marginHorizontal: 10,
  },
  filterView: {
    position: 'absolute',
    elevation: 11,
    padding: 15,
    borderRadius: 25,
    bottom: 10,
    right: 10,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: 'rgba(0,0,0,0.15)',
    shadowOpacity: 0.8,
    shadowRadius: 5,
    shadowOffset: {width: 1, height: 5},
  },
  filterIcon: {flexDirection: 'row', alignItems: 'center'},
  modelView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  titleModelText: {color: '#7D0552', fontSize: 20, fontWeight: 'bold'},
});

export default styles;
