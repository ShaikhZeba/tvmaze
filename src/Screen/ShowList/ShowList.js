import {
  View,
  Text,
  FlatList,
  RefreshControl,
  TouchableOpacity,
  Image,
  Dimensions,
  Button,
  TextInput,
  Keyboard,
} from 'react-native';
import { URLs } from '@networking/Urls';
import React, { useEffect, useRef, useState } from 'react';
import { callApi } from '@redux/CommonDispatch.js';
import LocalStyles from '@showList/ShowListStyles';
import { useDispatch, useSelector } from 'react-redux';
import { SHOW_LIST } from '@redux/Types';
import SearchIcon from '@icons/Search.svg';
import TvIcon from '@icons/tv.svg';
import BackIcon from '@icons/arrowFor.svg';
import moment from 'moment';
import FilterIcon from '@icons/Filter.svg';
import { Overlay } from 'react-native-elements';
import CloseIcon from '@icons/Cross.svg';
import {
  TvMazeTitle,
  NoDataTitle,
  RetryTitle,
  FilterTitle,
} from '@resources/String';
export default function ShowList({ navigation }) {
  const { showListData, showListLoading } = useSelector(
    (state) => state.ShowListReducer,
  ); //Redux Data
  const [showFilter, setShowFilter] = useState(false);
  const [is_Loading, setIs_Loading] = useState(true);
  const [countryValue, setCountryValue] = useState('');
  const dispatch = useDispatch(); //Action dispatch

  useEffect(() => {
    //Initial API Call
    getShowList();
  }, []);
  const getShowList = (value) => {
    let urlLinkObj = { ...URLs.SHOW_LIST };
   
    if (countryValue)
      urlLinkObj.URL = urlLinkObj.URL.replace("<<countryName>>", `country=${countryValue}&`)
    else
      urlLinkObj.URL = urlLinkObj.URL.replace("<<countryName>>", ``)

    console.log("urlLinkObj", urlLinkObj);
    dispatch(
      callApi(SHOW_LIST,urlLinkObj, {}, () => {
        //callApi fun use to call api with redux type , Url, body(Optional), isSuccessCallBack,isFailCallBack
        setCountryValue('');
        setShowFilter(false);
      }),
    );
  };

  const handleEmptyList = () => {
    //when data not found empty list view display by FlatList
    const { containerCenter, noDataImg, noDataText } = LocalStyles;
    if (showListLoading) {
      return false;
    } else {
      return (
        <View>
          <View style={containerCenter}>
            <Image
              style={noDataImg}
              source={require('@icons/AlertError.gif')}></Image>
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => {
                getShowList();
              }}>
              <View style={noDataText}>
                <Text style={{ fontSize: 18, color: '#7D0552' }}>
                  {NoDataTitle}
                </Text>
                <Text style={{ fontSize: 18, color: '#7D0552' }}>
                  {RetryTitle}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      );
    }
  };

  const {
    mainFlex,
    card,
    textStyles,
    headerContainer,
    titleText,
    paddingText,
    listContainer,
    listImg,
    textContainer,
    noDataImag,
    backView,
    filterView,
    filterIcon,
    modelView,
    titleModelText,
  } = LocalStyles;
  return (
    <View style={mainFlex}>
      <View style={headerContainer}>
        <Text style={titleText}>{TvMazeTitle}</Text>
        <TouchableOpacity style={paddingText}>
          <SearchIcon />
        </TouchableOpacity>
      </View>

      <FlatList //All Show list Data
        keyExtractor={(item, index) => 'ShowList' + index}
        data={showListData}
        showsVerticalIndicator={false}
        contentContainerStyle={listContainer}
        refreshControl={
          //Refresh flatList Data
          <RefreshControl
            colors={['#7D0552', '#7D0552']}
            refreshing={showListLoading}
            onRefresh={() => {
              getShowList();
            }}
          />
        }
        ListEmptyComponent={() => handleEmptyList()}
        renderItem={({ item, index }) => {
          return (
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => navigation.navigate('ShowDetails', { item: item })}
              style={card}>
              <View style={paddingText}>
                {item?.image?.medium ? (
                  <Image
                    source={{
                      uri: item?.image?.medium,
                    }}
                    style={listImg}
                    resizeMode={'stretch'}
                    onLoadStart={() => setIs_Loading(true)}
                    onLoadEnd={() => setIs_Loading(false)}
                  />
                ) : (
                  <View style={noDataImag}>
                    <TvIcon width={120} height={120} />
                  </View>
                )}
                <Text style={textStyles}>{item.name}</Text>
                <View style={textContainer}>
                  <Text style={textStyles}>
                    {'S'}
                    {item.season}
                    {' | '} {moment(item.airstamp).format('mm')}
                    {' min'}
                    {' | '} {moment(item.airdate).format('YY')}
                  </Text>
                  <View style={backView}>
                    <BackIcon width={15} height={15} />
                  </View>
                </View>
              </View>
            </TouchableOpacity>
          );
        }}
      />
      <TouchableOpacity //Filter Option
        activeOpacity={0.8}
        onPress={() => {
          setCountryValue('');
          setShowFilter(true);
        }}
        style={filterView}>
        <View style={filterIcon}>
          <FilterIcon />
        </View>
      </TouchableOpacity>

      <Overlay //For search
        isVisible={showFilter}
        height={'90%'}
        width={Dimensions.get('window').width * 0.9}
        onBackdropPress={() => {
          setCountryValue('');
          setShowFilter(false);
        }}>
        <View>
          <View style={modelView}>
            <Text style={titleModelText}>{FilterTitle}</Text>
            <TouchableOpacity
              onPress={() => {
                setCountryValue('');
                setShowFilter(false);
              }}>
              <CloseIcon width={20} height={20} />
            </TouchableOpacity>
          </View>
          <View style={{ ...paddingText, width: 200 }}>
            <TextInput
              onChangeText={(text) => setCountryValue(text)}
              value={countryValue}
              placeholder="Enter Country"
              onSubmitEditing={() => {
                Keyboard.dismiss();
                getShowList(countryValue);
              }}
            />
            <Button
              title={'Done'}
              color="#7D0552"
              onPress={() => {
                Keyboard.dismiss();
                setShowFilter(false);
                getShowList(countryValue);
              }}
            />
          </View>
        </View>
      </Overlay>
    </View>
  );
}
