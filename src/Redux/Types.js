export const SHOW_LIST_LOADING = 'Show_List_Loading';
export const SHOW_LIST_SUCCESS = 'Show_List_Success';
export const SHOW_LIST_FAILURE = 'Show_List_Failure';
export const SHOW_LIST = 'Show_List';
