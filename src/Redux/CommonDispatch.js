//Common dispatch for all action
import {apiCall as api} from '@networking/withAPI';
import { showSnackBar } from "@utils/Util";

export const callApi = ( 
  action,                //Action name to dispatch to reducer
  method,                //Endpoint of the URL
  body,                  //Data to pass in Body of the request
  isSuccess,             //callback function if API return success
  isFail,                //callback function if API return fail
  token,                 //JWT token (Optional)
) => async (dispatch) => {
  dispatch({type: action + '_Loading'});  //Dispatching Loading Action to reducer (Set loader to load on the page)

  let {apiSuccess, data} = await api(method, body, token); //calling API
  console.log('method', method);

  if (apiSuccess) {
    await dispatch({               //If API return success then dispatching Success Action with data returned from API
      type: action + '_Success',
      payload: {data},
    });
    isSuccess && isSuccess(data);
  } else {
    await dispatch({               //If API return fail then dispatching Failed Action with data(message) returned from API
      type: action + '_Failure',
      payload: {data},
    });
    showSnackBar(data.message, "error");

    isSuccess && isSuccess(data) //Success Callback to notify UI page that API returned Success with data 
    isFail && isFail(data);      //Failed Callback to notify UI page that API returned Failed with data(message)
  }
};
