import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import React from 'react';
import ShowList from '@showList/ShowList';
import ShowDetails from '@showDetails/ShowDetails';
//Stack navigation Implemented
const Stack = createStackNavigator();
function Navigator() {
  const MyStack = () => {
    // return two page (one by one)
    return (
      <Stack.Navigator>
        <Stack.Screen
          name="Show List"
          options={{headerShown: false}}
          component={ShowList}
        />
        <Stack.Screen
          name="ShowDetails"
          options={{headerShown: false}}
          component={ShowDetails}
        />
      </Stack.Navigator>
    );
  };
  return <NavigationContainer>{MyStack()}</NavigationContainer>;
}
export default Navigator;
